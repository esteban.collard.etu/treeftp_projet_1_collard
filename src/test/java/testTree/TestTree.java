package testTree;

import tree.*;

import static org.junit.Assert.*;

import org.junit.Test;


public class TestTree  {
	private class MockTree extends Tree{

		public MockTree() {}
		
		public String extractName(String donnee) {
			return super.extractName(donnee);
		}
		
		public Boolean isDirectory(String donnee) {
			return super.isDirectory(donnee);
		}
	}
	
	@Test
	public void testExtractName() {
		MockTree mt = new MockTree();
		assertEquals(mt.extractName("drwxr-xr-x    2 ftp      ftp          4096 Dec 24  2008 nzb"),"nzb");
	}
	
	@Test
	public void testIsDirectory() {
		MockTree mt = new MockTree();
		assertTrue(mt.isDirectory("drwxr-xr-x    2 ftp      ftp          4096 Dec 24  2008 nzb"));
		assertFalse(mt.isDirectory("lrwxrwxrwx    1 ftp      ftp            28 Jun 14  2011 MPlayer -> mirrors/mplayerhq.hu/MPlayer"));
	}
}
