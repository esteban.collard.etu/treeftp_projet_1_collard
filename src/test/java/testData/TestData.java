package testData;

import static org.junit.Assert.*;
import org.junit.Test;
import data.Data;


public class TestData  {
	private class MockData extends Data{

		public MockData(String nameData, int nbIndentation) {
			super(nameData, nbIndentation);
		}

		public String toString() {
			// TODO Auto-generated method stub
			return null;
		}
		
		protected String extractNameFromLink(String lien) {
			return super.extractNameFromLink(lien);
		}
	}
	
	@Test
	public void testextractNameFromLink() {
		MockData mt = new MockData("name", 23);
		assertEquals(mt.extractNameFromLink("/old-images/releases/lucid"),"lucid");
	}
}
