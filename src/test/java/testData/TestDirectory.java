package testData;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import data.Data;
import data.Directory;

public class TestDirectory {
	private class MockDirectory extends Directory{

		public MockDirectory(String nameData, int nbIndentation) {
			super(nameData, nbIndentation);
		}
		
		public List<Data> getList(){
			return this.listData;
		}
	}
	
	@Test
	public void testAddOnDirectory() {
		MockDirectory mt = new MockDirectory("name", 23);
		
		assertEquals(mt.getList().size(),0);
		mt.addOnDirectory(new Directory("name2", 24));
		assertEquals(mt.getList().size(),1);
	}
}
