package data;

import java.util.ArrayList;
import java.util.List;

/**
 * class Directory extension de la class 
 * @author collard
 */
public class Directory extends Data{
	
	/**
	 * List e de Data : contient l'ensemble des dossiers et fichier dans le dossier actuel
	 */
	protected List<Data> listData;

	/**
	 * Constructeur d'un repertoire
	 * @param nameData : nom du repertoire
	 * @param nbIndentation : nombre de dossier precedent ce repertoire
	 */
	public Directory(String nameData, int nbIndentation) {
		super(nameData, nbIndentation);
		this.listData = new ArrayList<Data>();
	}
	
	/**
	 * Ajoute dans le repertoire la Data passe en parametre
	 * @param element : Data a ajouter au repertoire
	 */
	public void addOnDirectory(Data element) {
		this.listData.add(element);
	}
	
	/**
	 * Retourne la representation du repertoire (Directory)
	 * @return la representation du repertoire
	 */
	public String toString() {
		String retVal = "";
		for (int i=0; i<this.nbIndentation; i++) {
			retVal += "    ";
		}
		retVal += extractNameFromLink(this.nameData)+"\n";
		for(Data d : this.listData) {
			retVal += d.toString();
		}

		return retVal;
	}

}
