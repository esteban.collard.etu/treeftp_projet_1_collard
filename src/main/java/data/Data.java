package data;

/**
 * abstract classe Data
 * @author collard
 */
public abstract class Data {
	
	/**
	 * nom du data
	 */
	protected String nameData;
	
	/**
	 * nombre de dossier precedent cette data
	 */
	protected int nbIndentation;
	
	public Data(String nameData, int nbIndentation) {
		this.nameData = nameData;
		this.nbIndentation = nbIndentation;
	}
	
	/**
	 * Retourne la representation du repertoire (Directory)
	 * @return la representation du repertoire
	 */
	public abstract String toString();
	
	/**
	 * retourne le nom de la data lien sans le nom des dossiers precedent
	 * @param lien : lien du fichier dossier dont le nom est a recuperer
	 * @return String : le nom du fichier ou dossier dans le lien
	 */
	protected String extractNameFromLink(String lien) {
		String[] tabLien = lien.split("/");
		return tabLien[tabLien.length-1];
	}

}
