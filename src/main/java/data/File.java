package data;

/**
 * class Directory extension de la class 
 * @author collard
 */
public class File extends Data{

	/**
	 * Constructeur d'un Fichier
	 * @param nameData : nom du Fichier
	 * @param nbIndentation : nombre de dossier precedent ce repertoire
	 */
	public File(String nameData, int nbIndentation) {
		super(nameData, nbIndentation);
	}

	/**
	 * Retourne la representation du fichier (File)
	 * @return la representation du repertoire
	 */
	public String toString() {
		String retVal = "";
		
		for(int i=0; i<this.nbIndentation; i++) {
			retVal += "    ";
		}
		return retVal + extractNameFromLink(this.nameData)+"\n";
	}

}
