package main;

import java.util.Scanner;
import ftp.ClientFtp;
import tree.Tree;


/**
 * Main class du programme Tree
 * @author collard
 */
public class Main {
	
	/**
	 * main fonction du programme Tree
	 * afficher a partir des parametres le dossier 
	 * @param args
	 */
	public static void main (String[] args){
		// Intitialisation variable
		int profondeurMax=-2;
		ClientFtp c = null;

		Scanner sc = new Scanner(System.in);
		System.out.println("Entrer la profondeur souhaiter (-1 si profondeur max) :");
		String pStr = sc.nextLine();
		sc.close();
		try {
			profondeurMax = Integer.parseInt(pStr);
			if(profondeurMax<-1) {
				System.out.println("Nombre non valide\n");
				return;
			}
		}
		catch(NumberFormatException e) {
			System.out.println("Entrer non valide (Ce n'est pas un nombre)");
			return;
		}

		// Creation du Client FTP
		switch(args.length) {
		case 0:
			c = new ClientFtp("ubuntu",21);
			break;
		case 1:
			c = new ClientFtp(args[0], 21);
			break;
		case 2:
			c = new ClientFtp(args[0], 21, args[1]);
			break;
		case 3:
			c = new ClientFtp(args[0], 21, args[1], args[2]);
			break;
		default:
			printUsage();
			return;
		}

		// Connexion
		Tree t = new Tree(c, profondeurMax);
		t.affichage();

	}


	/**
	 * affiche la methode d'utilisation du programme
	 */
	private static void printUsage() {
		System.out.println("Usage: java -jar target/Projet1-1.0-SNAPSHOT.jar adresse_serveur_ftp [username] [password]");
	}
}
