package ftp;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Class fesant la relation entre notre machine et un serveur FTP
 * @author collard
 */
public class ClientFtp {

	/**
	 * Nom du serveur ftp
	 */
	private String serveur;

	/**
	 * Port utiliser pour la connexion au serveur FTP
	 */
	private int port;

	/**
	 * Nom d'utilisateur utiliser pour la connexion au serveur FTP
	 */
	private String user;

	/**
	 * Mot de passe utiliser pour la connexion au serveur FTP
	 */
	private String password;

	/**
	 * Socket utiliser pour la connexion au serveur FTP
	 */
	private Socket socket;

	/**
	 * Buffer reader utiliser pour la connexion au serveur FTP
	 */
	private BufferedReader reader;

	/**
	 * Buffer writer utiliser pour la connexion au serveur FTP
	 */
	private BufferedWriter writer;

	/**
	 * Message reçu par le serveur FTP
	 */
	private String message;

	/**
	 * Construit la connexion au serveur ftp a partir du nom du serveur et du port
	 * @param serveur : nom du serveur
	 * @param port : port utiliser pour la connexion au serveur
	 */
	public ClientFtp(String serveur, int port) {
		this.serveur = serveur;
		this.port = port;
		this.user = null;
		this.password = null;
		this.socket = null;
		this.reader = null;
		this.writer = null;
		this.message = "";
	}

	/**
	 * Construit la connexion au serveur ftp a partir du nom du serveur, du port et de l'identifiant
	 * @param serveur : nom du serveur
	 * @param port : port utiliser pour la connexion au serveur
	 * @param user : nom d'utilisateur utiliser pour la connexion au serveur
	 */ 
	public ClientFtp(String serveur, int port, String user) {
		this.serveur = serveur;
		this.port = port;
		this.user = user;
		this.password = null;
		this.socket = null;
		this.reader = null;
		this.writer = null;
		this.message = "";
	}

	/**
	 * Construit la connexion au serveur ftp a partir du nom du serveur, du port, de l'identifiant et du mot de passe
	 * @param serveur : nom du serveur
	 * @param port : port utiliser pour la connexion au serveur
	 * @param user : nom d'utilisateur utiliser pour la connexion au serveur
	 * @param password : mot de passe utiliser pour la connexion au serveur
	 */ 
	public ClientFtp(String serveur, int port, String user, String password) {
		this.serveur = serveur;
		this.port = port;
		this.user = user;
		this.password = password;
		this.socket = null;
		this.reader = null;
		this.writer = null;
		this.message = "";
	}

	/**
	 * Connexion au serveur FTP
	 */
	public void connection() {
		try {
			this.socket = new Socket(this.serveur,this.port);
			this.reader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
			this.writer = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));


			message = readMessage();

			if (!message.startsWith("220 ")) {

				throw new IOException(
						"SimpleFTP received an unknown response when connecting to the FTP server: "
								+ message);
			}

			sendUser();
			message = readMessage();
			if (!message.startsWith("331 ")) {	
				throw new IOException("SimpleFTP received an unknown response after sending the user: "+ message);
			}

			sendPass();
			message = readMessage();
			if (!message.startsWith("230 ")) {
				throw new IOException("SimpleFTP was unable to log in with the supplied password: "+ message);
			}

		}
		catch(UnknownHostException e) {
			System.out.print("UnknownHostException\n");
			return;
		}
		catch(IOException e) {
			System.out.print("IOException\n");
			return;
		}

	}

	/**
	 * Deconnexion du serveur ftp
	 */
	public void deconnexion() {
		try {
			this.reader.close();
			this.writer.close();
			this.socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Recupere les fichiers et dossier dans le repertoire passe en parametre
	 * @param directory : dossier a explorer
	 * @return String contenant le contenu du dossier passé en paramètre
	 */
	public String getFileOnDirectory(String directory) {
		try {
			changeDirectory(directory);

			sendPasv();
			//227 Entering Passive Mode (....)
			message = readMessage();
			if(message != null) {

				String[] resConnection = message.substring(message.indexOf("(")+1,message.indexOf(")")).split(",");

				Socket socketTest = new Socket(resConnection[0]+"."+resConnection[1]+"."+resConnection[2]+"."+resConnection[3],
						(Integer.parseInt(resConnection[4])*256 + Integer.parseInt(resConnection[5]))
						);

				BufferedReader reader2 = new BufferedReader(new InputStreamReader(socketTest.getInputStream()));
				sendList(null);

				//150 Here comes the directory listing.
				message = readMessage();

				//226 Directory send OK.
				message = readMessage();

				String valRet = "";
				String tempVal;
				while((tempVal = reader2.readLine()) != null) {
					valRet += tempVal+"\n";
				}

				reader2.close();
				socketTest.close();
				

				return valRet;
			}
			return "";

		} catch (IOException e) {
			e.printStackTrace();
			return "Error";
		}
	}

	/**
	 * Fonction permetant de changer de repertoire courant sur le serveur
	 * @param lien sur le quel vous souhaiter vous deplacer
	 */
	private void changeDirectory(String lien) {
		try {
			sendCwd(lien);
			message = readMessage();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Envoye l'identifiant au serveur ftp
	 * @throws IOException
	 */
	private void sendUser() throws IOException {
		if(this.user == null) {
			sendMessage("USER anonymous");
		}
		else {
			sendMessage("USER "+this.user);
		}
	}

	/**
	 * Envoye le mot de passe au serveur ftp
	 * @throws IOException
	 */
	private void sendPass() throws IOException {
		if(this.user == null) {
			sendMessage("PASS anonymous@domain.com");
		}
		else {
			sendMessage("PASS "+this.password);
		}
	}

	/**
	 * Permet d'envoyer la message PASV au serveur ftp
	 * Cette commande permet de se connecter en mode passif. 
	 * @throws IOException
	 */
	private void sendPasv() throws IOException {
		sendMessage("PASV");
	}

	/**
	 * Envoi le message LIST au serveur ftp
	 * Affiche les informations d'un fichier ou d'un repertoire specifique, ou du repertoire courant. 
	 * @param parameter : repertoire specifique, null si repertoire courant
	 * @throws IOException
	 */
	private void sendList(String parameter) throws IOException {
		if(parameter == null) {
			sendMessage("LIST");    		
		}
		else {

			sendMessage("LIST "+parameter); 
		}
	}

	/**
	 * Envoi le message CWD au serveur ftp
	 * Change de repertoire courant
	 * @param parameter : repertoire specifique, null si repertoire courant
	 * @throws IOException
	 */
	private void sendCwd(String chemin) throws IOException {
		if(chemin == null) {
			sendMessage("CWD");
		}
		else {
			sendMessage("CWD "+chemin); 
		}
	}

	/**
	 * Permet d'envoyer un message au serveur ftp
	 * @param message : message a envoyer au serveur ftp
	 * @throws IOException
	 */
	private void sendMessage(String message) throws IOException {
		if (socket == null) {
			throw new IOException("SimpleFTP is not connected.");
		}
		try {
			this.writer.write(message + "\r\n");
			this.writer.flush();
		} catch (IOException e) {
			socket = null;
			throw e;
		}
	}

	/**
	 * Permet de recuperer le message envoyer par le serveur ftp
	 * @return String valeur envoyer par le serveur ftp
	 * @throws IOException
	 */
	private String readMessage() throws IOException {
		return this.reader.readLine();
	}
}
