package tree;

import ftp.ClientFtp;
import data.*;


/**
 * Classe Tree 
 * @author Collard Esteban
 *
 */
public class Tree {

	/**
	 * client ftp de la commande tree
	 */
	private ClientFtp clientFtp;
	
	/**
	 * repertoire contenant l'ensemble du dossier a afficher
	 */
	private Directory dir;
	
	/**
	 * profondeur max de l'arbre a stoker et a afficher
	 */
	private int profondeurMax;
	
	/**
	 * Constructeur pour la classe de test
	 */
	protected Tree() {
		this.clientFtp = null;
	}

	/**
	 * Construit l'objet Tree contenant l'ensemble des donnees du dossier courant du serveur ftp
	 * @param clientFtp : client ftp etablissant la connexion avec le serveur ftp
	 */
	public Tree(ClientFtp clientFtp) {
		this.clientFtp = clientFtp;
		this.clientFtp.connection();
		this.profondeurMax = -1;   // Valeur pour avoir une profondeur
		this.dir = remplissageData("/cdimage/bionic/daily-live",0);
		this.clientFtp.deconnexion();
	}

	/**
	 * Construit l'objet Tree contenant l'ensemble des donnees du dossier courant du serveur ftp
	 * @param clientFtp : client ftp etablissant la connexion avec le serveur ftp
	 * @param profondeurMax : profondeur max de l'arbre a stoker et a afficher
	 */
	public Tree(ClientFtp clientFtp, int profondeurMax) {
		this.clientFtp = clientFtp;
		this.clientFtp.connection();
		this.profondeurMax = profondeurMax;
		this.dir = remplissageData("",0);
		this.clientFtp.deconnexion();
	}

	/**
	 * 
	 * @param lien : position actuel du dossier a retourner sur le serveur ftp
	 * @param nbIndentation : nombre de dossier precedent ce lien
	 * @return Directory : retourne l'ensemble des dossiers et fichier a partir du lien
	 */
	private Directory remplissageData(String lien, int nbIndentation) {
		Directory dirRet = new Directory(lien, nbIndentation);
		
		if(this.profondeurMax==-1 || this.profondeurMax>nbIndentation) {
			String fileGet = this.clientFtp.getFileOnDirectory(lien);
			if(fileGet.length()!=0) {
				String[] tabFileGet = fileGet.split("\n");
				for(int i=0; i<tabFileGet.length; i++) {
					if(isDirectory(tabFileGet[i])) {
						dirRet.addOnDirectory(remplissageData(lien+"/"+extractName(tabFileGet[i]),nbIndentation+1));
					}
					else {
						dirRet.addOnDirectory(new File(lien+"/"+extractName(tabFileGet[i]), nbIndentation+1));
					}
				}
			}
		}
		return dirRet;
	}

	/**
	 * retourne le nom du dossier ou fichier a partir d'une ligne envoyer par le serveur
	 * @param donnee : ligne envoyer du serveur
	 * @return String : retourne le nom du fichier ou dossier
	 */
	protected String extractName(String donnee) {
		String[] tabTestdonnee = donnee.split(" ");
		return tabTestdonnee[tabTestdonnee.length-1];
	}

	/**
	 * retourne vrai si la ligne du serveur correspond a un dossier faux sinon
	 * @param donnee : la ligne du fichier ou dossier envoyer par le serveur
	 * @return Boolean : retourne vrai si la ligne du serveur correspond a un dossier faux sinon
	 */
	protected Boolean isDirectory(String donnee) {
		return donnee.charAt(0)=='d';
	}

	/**
	 * Affiche l'arbre
	 */
	public void affichage() {
		System.out.print("\n/"+this.dir);
	}
}
