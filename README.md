# Tree FTP

Esteban Collard
4/02/2021

## Introduction


L'objectif du projet est de réaliser une commande tree à partir d'un répertoire distant. Pour accéder au répertoire nous utilisont le protocole FTP.

## Compilation
`mvn package` permet de compiler le code java

## Javadoc
`mvn javadoc:javadoc` permet de généré la documentation des classes

## Exécution
`java -jar target/Projet1-1.0-SNAPSHOT.jar` suivi du nom du serveur ftp et d'autre paramètre optionnel, permet d'exécuter le programme.
Les paramètres optionnels sont les suivants :
	* nom d'utilisateur (optionnel)
	* mot de passe (optionnel)

Des le début de l'excution du programme va vous demander d'entrer une profondeur maximale dans la recherche des dossiers.
Si vous répondez -1 le parcours des dossiers ce fera jusqu'a la fin.

Si vous le souhaiter vous avez une démo de l'exécution du programme dans le dossier docs

## Architecture du projet

### Diagramme de classe

Voici le diagramme de classe du projet

![Diagramme de classe](docs/Diagramme de classe Tree.png)

La projet ce distingue en plusieur classe :
	* la classe ClientFtp qui s'occupe d'établir une connexion entre le serveur ftp et notre machine
	* la classe Data composé de 2 sous classses Directory et File qui s'occupe de stocké l'ensemble des répertoires et fichiers
	* la classe Tree qui s'occupe de faire la liaison entre les classes ClientFtp et Data


### Gestion d'erreurs

Les erreurs sont gérer via un system de try catch. Dans le cas où une erreurs est détecté elle est ensuite traité dans le catch correspondant à l'erreur, en voici un exemple.

		try {
			this.reader.close();
			this.writer.close();
			this.socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

L'autre type d'erreur que nous gérons est le cas où le serveur n'envoie pas un message valide au client. Il est ainsi récupérer est traité en conséquence.
Voici un exemple lors de la connexion au serveur.

message = readMessage();

		if (!message.startsWith("220 ")) {

			throw new IOException(
					"SimpleFTP received an unknown response when connecting to the FTP server: "
					+ message);
		}
			

## Code samples

Cette fonction permet d'envoyer au serveur ftp n'importe qu'elle message.

		private void sendMessage(String message) throws IOException {
			if (socket == null) {
				throw new IOException("SimpleFTP is not connected.");
			}
			try {
				this.writer.write(message + "\r\n");
				this.writer.flush();
			} catch (IOException e) {
				socket = null;
				throw e;
			}
		}
	

Cette fonction permet de remplir les données Data

		private Directory remplissageData(String lien, int nbIndentation) {
			Directory dirRet = new Directory(lien, nbIndentation);
			
			if(this.profondeurMax==-1 || this.profondeurMax>nbIndentation) {
				String fileGet = this.clientFtp.getFileOnDirectory(lien);
				if(fileGet.length()!=0) {
					String[] tabFileGet = fileGet.split("\n");
					for(int i=0; i<tabFileGet.length; i++) {
						if(isDirectory(tabFileGet[i])) {
							dirRet.addOnDirectory(remplissageData(lien+"/"+extractName(tabFileGet[i]),nbIndentation+1));
						}
						else {
							dirRet.addOnDirectory(new File(lien+"/"+extractName(tabFileGet[i]), nbIndentation+1));
						}
					}
				}
			}
			return dirRet;
		}
		
## Question bonus

Dans ce projet, la question bonus concernant le nombre de sous répertoire à été traité.
Concernant la question sur la reconnexion, ici elle n'a pas été intégralement traité. En effet ici elle peut être possible car pour chaque dossier récupéré est stocké, ce qui pourrait permetre en cas de rupture de la connexion de savoir où nous en étions.
